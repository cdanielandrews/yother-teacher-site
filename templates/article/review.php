{embed="includes/doc_header" title="{exp:weblog:entries weblog="home|grade11|grade11honors|reminders" limit="1" disable="member_data|trackbacks" show_future_entries="yes"}{title} &mdash; Due: {entry_date format="%D, %F %d, %Y"}{/exp:weblog:entries}"}

			
			<div id="content">
				<div id="left">
				
{exp:weblog:entries weblog="grade11|grade11honors" status="open|Featured" show_future_entries="yes" limit="20" disable="member_data|trackbacks"}
	<h1>{title}</h1>
	<p><strong>Due: {entry_date format="%D, %F %d, %Y"}</strong></p>
	{body}
{/exp:weblog:entries}
					
					
				</div> <!-- end left -->
				<div id="right">
					{embed="includes/doc_sidebar"}

				</div> <!-- end right -->
			
			</div> <!-- end content -->
			

{embed="includes/doc_footer"}

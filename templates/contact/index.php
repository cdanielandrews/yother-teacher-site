{embed="includes/doc_header" title="Contact Mrs. Andrews" loc="contact"}

			
			<div id="content">
				<div id="left">
<h1>Contact Mrs. Andrews</h1>			

<p>Questions?  Concerns?  Contact me!  I will typically reply within 24 hours, unless it is a weekend/holiday.  Please remember to include your name (if you are a parent, please also include your child's name), e-mail address, and questions/comments.  Thanks!</p>

<form name="form1" method="post" action="/contact/thanks/" id="contactform">
	      <table width="100%" border="0" cellspacing="5" cellpadding="0">
            <tr>
              <td valign="top"><div align="right">Name:</div></td>
              <td valign="top"><input type="text" name="name" id="name"></td>
            </tr>
            <tr>
              <td valign="top"><div align="right">Email Address:</div></td>
              <td valign="top"><input type="text" name="email" id="email"></td>
            </tr>
            <tr>
              <td valign="top"><div align="right">Comments:</div></td>
              <td valign="top"><textarea name="comments" id="comments" rows="8"></textarea></td>
            </tr>
            <tr>
            	<td></td>
            	<td><br/><input type="submit" value="Send Email!" name="submit" class="submit"></td>
            </tr>
          </table>
	      </form>

</div> <!-- end left -->
				<div id="right">
					{embed="includes/doc_sidebar"}

				</div> <!-- end right -->
			
			</div> <!-- end content -->
			

{embed="includes/doc_footer"}
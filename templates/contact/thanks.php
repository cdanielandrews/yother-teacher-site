{embed="includes/doc_header" title="Contact Mrs. Andrews" loc="contact"}

			
			<div id="content">
				<div id="left">
<h1>Contact Mrs. Andrews</h1>			
<?php
	$name = $_POST['name'];
	$email = $_POST['email'];
	$comments = $_POST['comments'];
	
	if(!empty($email) && !empty($name) && !empty($comments)){
	
	$to      = 'AndrewsMY@fultonschools.org';
	$subject = 'Contact Form Submission';
	$message = "Name: $name \nEmail: $email \n\nComments: $comments";	
	$headers = "From: $email";

	mail($to, $subject, $message, $headers);
	
	echo "<h1>Thanks!</h1>";

	echo "<p>Your message has been received and I will be in contact with you shortly.</p>";
	
	}else{
	
	echo "<h1>Error</h1>";

	echo "<p>Your name, email address, and a comment are required to submit the form.  Please <a href=\"/contact\">go back</a> and try again.</p>";	
	
	}

?>


</div> <!-- end left -->
				<div id="right">
					{embed="includes/doc_sidebar"}

				</div> <!-- end right -->
			
			</div> <!-- end content -->
			

{embed="includes/doc_footer"}
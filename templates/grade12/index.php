{embed="includes/doc_header" title="12th Grade" loc="12th"}

			
			<div id="content">
				<div id="left">

<h1>Welcome to 12th Grade!</h1>
<p>The tools below will help you stay up-to-date in the class. &#160;You may get information for a specific day on the&#160;Assignment Calendar&#160;or browse general topics using the&#160;Recent Assignments&#160;section.</p>

<h2>Assignment Calendar</h2>
<div id="calendar">
{exp:weblog:calendar switch="calendarToday|calendarCell" weblog="grade12" show_future_entries="yes"}

<table class="calendarBG" border="0" cellpadding="5" cellspacing="0" summary="My Calendar">
<tr class="calendarHeader">
<th><div class="calendarMonthLinks"><a href="{previous_path=grade12/review}">&lt;&lt;</a></div></th>
<th colspan="5" align="center">{date format="%F %Y"}</th>
<th><div class="calendarMonthLinks"><a class="calendarMonthLinks" href="{next_path=grade12/review}">&gt;&gt;</a></div></th>
</tr>
<tr>
{calendar_heading}
<td class="calendarDayHeading">{lang:weekday_abrev}</td>
{/calendar_heading}
</tr>

{calendar_rows }
{row_start}<tr>{/row_start}

{if entries}
<td class='{switch}' align='center'><a href="{day_path=grade12/review}">{day_number}</a></td>
{/if}

{if not_entries}
<td class='{switch}' align='center'>{day_number}</td>
{/if}

{if blank}
<td class='calendarBlank'>{day_number}</td>
{/if}

{row_end}</tr>{/row_end}
{/calendar_rows}
</table>
{/exp:weblog:calendar}
</div><!-- calendar -->

<p>Click on the calendar to find out what we did in class on a specific day.  If you have questions about the assignments or documents, please <a href="mailto:AndrewsMY@fultonschools.org">e-mail me</a> or drop by my classroom before school.  I'll do my best to keep this calendar up-to-date, but if you cannot find the information that you need, please ask!
</p>


<h2 style="clear:both;">Recent Assignments</h2>

{exp:weblog:entries weblog="grade12" orderby="date" sort="desc" limit="20" show_future_entries="yes" status="open|Featured" disable="member_data|trackbacks"}
	{entry_date format="%m.%d.%Y"} | <strong><a href="{title_permalink="article"/view}">{title}</a></strong><br/>
	
{/exp:weblog:entries}


</div> <!-- end left -->
				<div id="right">
					{embed="includes/doc_sidebar"}

				</div> <!-- end right -->
			
			</div> <!-- end content -->
			

{embed="includes/doc_footer"}

{embed="includes/doc_header" loc="grade12" title="Assignment Archive"}


			
			<div id="content">
				<div id="left">
				
{exp:weblog:entries weblog="grade12" status="open|Featured" show_future_entries="yes" limit="20" disable="member_data|trackbacks"}
	<h1>{title}</h1>
	<p><strong>Due: {entry_date format="%D, %F %d, %Y"}</strong></p>
	{body}
{/exp:weblog:entries}
					
					
				</div> <!-- end left -->
				<div id="right">
					{embed="includes/doc_sidebar"}

				</div> <!-- end right -->
			
			</div> <!-- end content -->
			

{embed="includes/doc_footer"}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title>{embed:title} &mdash; Michelle Y. Andrews</title>
		<link rel="stylesheet" type="text/css" href="/main.css" />
		<meta name="description" content="Michelle Andrews is an English teacher at Alpharetta High School." />
		<meta name="keywords" content="Alpharetta High School, Alpharetta, Georgia, Michelle Andrews, English, Teacher" />

		
		
	</head>
	<body>
		<div id="container">
			<div id="header">
				<a href="/"><img src="/images/yother/header.jpg" alt="Michelle Y. Andrews" width="800" height="150" border="0" /></a>
			</div> <!-- end header -->
				<ul id="menubar">
					<li id="home"><a {if '{embed:loc}' == 'home'}class="current"{/if}href="/"><span class="hiddentext">home</span></a></li>
					<li id="aplang"><a {if '{embed:loc}' == 'aplang'}class="current"{/if}href="/aplang" ><span class="hiddentext">AP Lang</span></a></li>
					<li style="display:none" id="twelve"><a {if '{embed:loc}' == '12th'}class="current"{/if}href="/grade12" ><span class="hiddentext">12th grade</span></a></li>
					<li style="display:none" id="eleven"><a {if '{embed:loc}' == '11th'}class="current"{/if}href="/grade11"><span class="hiddentext">11th grade</span></a></li>
					<li id="contact"><a {if '{embed:loc}' == 'contact'}class="current"{/if}href="/contact"><span class="hiddentext">contact</span></a></li>
					<li id="links"><a {if '{embed:loc}' == 'links'}class="current"{/if}href="/links"><span class="hiddentext">links</span></a></li>
				</ul><!-- menubar -->				
							

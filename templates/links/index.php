{embed="includes/doc_header" title="Links" loc="links"}

			
			<div id="content">
				<div id="left">
<img src="/images/yother/welcome-links.gif" alt="welcome-links" width="380" height="54"/>
					<p>Here are some links that you should check out!</p>

{exp:weblog:entries weblog="links" orderby="title" sort="asc"  disable="member_data|trackbacks"}
	<div class="{switch="white|green"}">	
	<h3><a href="{link_url}">{title}</a></h3>
	<p>{link_description}</p>
	<p><a href="{link_url}" title="{link_url}">Visit the site &raquo;</a> </p>
	</div>
{/exp:weblog:entries}
					
					
				</div> <!-- end left -->
				<div id="right">
					{embed="includes/doc_sidebar"}

				</div> <!-- end right -->
			
			</div> <!-- end content -->
			

{embed="includes/doc_footer"}
